package com.website.springmvc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.website.springmvc.entities.Address;
import com.website.springmvc.entities.Course;
import com.website.springmvc.entities.Student;
import com.website.springmvc.service.AddressService;
import com.website.springmvc.service.CourseService;
import com.website.springmvc.service.StudentService;

@Controller
@RequestMapping(value = "/studentcontroller")
public class StudentController {
	@Autowired
	private StudentService studentService;
	@Autowired
	private CourseService courseService;
	@Autowired
	SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/students", method = RequestMethod.GET)
	public ModelAndView listStudent(@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();

		model.setViewName("studentList");
		model.addObject("students", studentService.getAll());
		return model;
	}

	@RequestMapping(value = "/addStudent", method = RequestMethod.GET)
	public ModelAndView addUpdate() {
		ModelAndView model = new ModelAndView();

		model.setViewName("studentDetail");
		model.addObject("student", new Student());
		model.addObject("courses", courseService.getAll());
		model.addObject("mode", "ADD");
		return model;
	}

	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public ModelAndView viewStudent(@RequestParam Long id, @RequestParam String mode) {
		ModelAndView model = new ModelAndView();
		model.setViewName("studentDetail");
		model.addObject("student", studentService.get(id));

		// Em lấy list course là để cho user có muốn thêm môn hay không á anh
		model.addObject("courses", courseService.getAll());
		model.addObject("mode", mode);
		return model;
	}

	@RequestMapping(value = "/student", method = RequestMethod.POST)
	public String btnSave(@ModelAttribute Student student, @RequestParam String btnSave) {

		List<Course> list = new ArrayList<>();
		String[] ids = btnSave.split(",");
		for (int i = 0; i < ids.length; i++) {
			list.add(courseService.get((Long.parseLong(ids[i]))));
		}
		student.setCourses(list);
		System.out.println("id" + student.getId());
		if (student.getId() == null) {
			studentService.add(student);
			System.out.println("added");
		} else {
			student.setCourses(list);
			studentService.update(student);
		}
		return "redirect:/studentcontroller/students";
	}

	@RequestMapping(value = "/student/{id}", method = RequestMethod.DELETE)
	public void btnDelete(@PathVariable(value = "id") Long id) {
		Student student = studentService.get(id);
		studentService.delete(student);
	}
}
