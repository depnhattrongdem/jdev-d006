package demo.hibernate;

import java.util.ArrayList;
import java.util.List;

import hibernate.dao.AddressDAO;
import hibernate.dao.ClazzDAO;
import hibernate.dao.CourseDAO;
import hibernate.dao.StudentDAO;
import hibernate.entity.Address;
import hibernate.entity.Clazz;
import hibernate.entity.Course;
import hibernate.entity.Student;

public class HibernateTesting {

	public static void main(String[] args) {
		// Inital Hibernate
		AddressDAO dao = new AddressDAO();
//		testUpdateStudentJoinCourse();
//		testInsertStudent();
		// testGetStudentById(1);
		// testDeleteStudent();
		// testUpdateStudent();
		// testInsertClass();
		// testInsertCourse();
	}

	public static void testUpdateStudentJoinCourse() {
		CourseDAO courseDAO = new CourseDAO();
		StudentDAO studentDAO = new StudentDAO();
		
		try {
			Student student = studentDAO.get(1);
			Course course = courseDAO.get(1);
			List<Course> courses = new ArrayList<Course>();
			courses.add(course);
			student.course = courses;
			studentDAO.update(student);
			System.out.println("testUpdateStudentJoinCourse successful: " + student.toString());
		} catch (Exception e) {
			System.out.println("testUpdateStudentJoinCourse failed: " + e.getMessage());
		}
	}

	public static void testInsertCourse() {
		try {
			CourseDAO courseDAO = new CourseDAO();
			Course course = new Course();
			course.name = "JDEV-D006-Spring-Framework";
			courseDAO.insert(course);
			System.out.println("Insert Course object successful: " + course.toString());
		} catch (Exception e) {
			System.out.println("Insert Course object failed: " + e.getMessage());
		}
	}

	public static void testInsertClass() {
		try {
			ClazzDAO dao = new ClazzDAO();
			Clazz clazz = new Clazz();
			clazz.name = "JDEV-D006";
			dao.insert(clazz);
			System.out.println("Insert Class object successful: " + clazz.toString());
		} catch (Exception e) {
			System.out.println("Insert Class object failed: " + e.getMessage());
		}
	}

	public static void testUpdateStudent(long id) {
		try {
			StudentDAO studentDAO = new StudentDAO();
			Student student = studentDAO.get(id);
			student.age = 40;
			studentDAO.update(student);
			System.out.println("Update Student object successful: " + student.toString());
		} catch (Exception e) {
			System.out.println("Update Student object failed: " + e.getMessage());
		}
	}

	public static void testDeleteStudent() {
		try {
			StudentDAO studentDAO = new StudentDAO();
			Student student = studentDAO.get(2);
			studentDAO.delete(student);
			System.out.println("Delete Student object successful: " + student.toString());
		} catch (Exception e) {
			System.out.println("Delete Student object failed: " + e.getMessage());
		}
	}

	public static void testInsertStudent() {

		try {
			StudentDAO studentDAO = new StudentDAO();
			ClazzDAO clazzDAO = new ClazzDAO();
			CourseDAO courseDAO = new CourseDAO();

			Address add = new Address();
			add.city = "HCM";
			add.district = "3";
			add.street = "Nguyen Thi Minh Khai";
			Clazz clazz = clazzDAO.get(1);
			Student student = new Student();
			student.address = add;
			student.age = 30;
			student.clazz = clazz;
			student.lastName = "Vo";
			student.firstName = "Thach";

			studentDAO.insert(student);
			System.out.println("Insert Student object successful: " + student.toString());
		} catch (Exception e) {
			System.out.println("Insert Student object failed: " + e.getMessage());
		}
	}

	public static void testGetStudentById(long id) {
		StudentDAO dao = new StudentDAO();
		Student result = dao.get(id);
		System.out.println("Get student by id=%s: " + result.toString());
	}
}
