package hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="course", catalog="website")
public class Course {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name", length = 100)
	public String name;
	
	public Course() {
		// TODO Auto-generated constructor stub
	}
		
	public String toString() {
		return String.format("{id: %s, name: %s}", id, name);
	}
}
