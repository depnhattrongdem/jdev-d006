package hibernate.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="class", catalog="website")
public class Clazz implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(name = "name", length = 100, nullable = false)
	public String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "clazz", cascade=CascadeType.ALL)
	public List<Student> students;
	
	public Clazz() {
		// TODO Auto-generated constructor stub
	}

	public String toString() {
		return String.format("{id: %s, name: %s}", id, name);
	}
}
