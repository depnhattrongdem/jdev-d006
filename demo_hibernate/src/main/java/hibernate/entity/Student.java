package hibernate.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student", catalog = "website")
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public long id;

	@Column(name = "first_name", length = 20)
	public String firstName;

	@Column(name = "last_name", length = 20)
	public String lastName;

	@Column(name = "age")
	public int age;

	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="address_id")
	public Address address;

	@ManyToOne
	@JoinColumn(name="class_id", nullable=false)
	public Clazz clazz;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
			name="student_course", 
			joinColumns={@JoinColumn(name="student_id")}, 
			inverseJoinColumns={@JoinColumn(name="course_id")}
			)
	public List<Course> course;
	
	public Student() {
		// TODO Auto-generated constructor stub
	}
	
	public String toString() {
		return String.format("{id:%s,firstName:%s,lastName:%s,age:%s,address:%s}", id, firstName, lastName, age,
				address.toString());
	}
}
