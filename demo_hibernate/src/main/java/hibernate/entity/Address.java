package hibernate.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="address", catalog="website")
public class Address implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public long id;
	
	@Column(name="street", length=100)
	public String street;
	
	@Column(name="district", length=100)
	public String district;
	
	@Column(name="city", length=100)
	public String city;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Address() {
		// TODO Auto-generated constructor stub
	}
	
	public String toString(){
		return String.format("{id:%s,street:%s,district:%s,city:%s}", id, street, district, city);
	}
}
